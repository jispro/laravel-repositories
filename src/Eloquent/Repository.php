<?php namespace Jispro\Repositories\Eloquent;

use Jispro\Repositories\Contracts\CriteriaInterface;
use Jispro\Repositories\Criteria\Criteria;
use Jispro\Repositories\Contracts\RepositoryInterface;
use Jispro\Repositories\Exceptions\RepositoryException;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;

/**
 * Class Repository
 * @package Jispro\Repositories\Eloquent
 */
abstract class Repository implements RepositoryInterface, CriteriaInterface {

	/**
	 * @var App
	 */
	private $app;

	/**
	 * @var \Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * @var
	 */
	protected $modelName;

	/**
	 * @var Collection
	 */
	protected $criteria;

	/**
	 * @var bool
	 */
	protected $skipCriteria = false;

	/**
	 * @param App $app
	 * @param Collection $collection
	 * @throws \Jispro\Repositories\Exceptions\RepositoryException
	 */
	public function __construct(App $app, Collection $collection) {
		$this->app = $app;
		$this->criteria = $collection;
		$this->resetScope();
		$this->model = $this->makeModel($this->model());
		$this->init();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Model
	 */
	public function getModel()
	{
		return $this->model;
	}

	/**
	 * Specify Model class name
	 *
	 * @return mixed
	 */
	abstract function model();

	/**
	 * @param array 	$columns
	 * @param boolean 	$trashed
	 * @return mixed
	 */
	public function all($columns = array('*'), $trashed=false) {
		$this->applyCriteria();

		if($trashed==true) return $this->model->withTrashed()->get($columns);
		return $this->model->get($columns);
	}

	/**
	 * @param int 		$perPage
	 * @param array 	$columns
	 * @param boolean 	$trashed
	 * @return mixed
	 */
	public function paginate($perPage = 15, $columns = array('*'), $trashed=false) {
		$this->applyCriteria();

		if($trashed==true) return $this->model->withTrashed()->paginate($perPage, $columns);
		return $this->model->paginate($perPage, $columns);
	}

	/**
	 * @param array $data
	 * @param boolean $guarded
	 * @return mixed
	 */
	public function create(array $data, $guarded=true) {

		if($guarded == false){
			$this->model->unguard();
			$model =  $this->model->create($data);
			$this->model->reguard();
		}else{
			$model =  $this->model->create($data);
		}

		return $model;
	}

	/**
	 * @param array $data
	 * @param $id
	 * @param string $attribute
	 * @return mixed
	 */
	public function update(array $data, $id, $attribute="id") {
		return $this->model->where($attribute, '=', $id)->update($data);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function delete($id) {
		return $this->model->destroy($id);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function restore($id) {
		return $this->find($id, array('id'), true)->restore();
	}

	/**
	 * @param 			$id
	 * @param array 	$columns
	 * @param boolean 	$trashed
	 * @return mixed
	 */
	public function find($id, $columns = array('*'), $trashed=false) {
		$this->resetModel();
		$this->applyCriteria();

		if($trashed==true) return $this->model->withTrashed()->find($id, $columns);
		return $this->model->find($id, $columns);
	}

	/**
	 * @param 			$attribute
	 * @param 			$value
	 * @param 			$method
	 * @param array 	$columns
	 * @param boolean 	$trashed
	 * @param boolean 	$first
	 * @return mixed
	 */
	public function findBy($attribute, $value, $method='=', $columns = array('*'), $trashed=false, $first = true) {

		$this->model = $this->app->make($this->modelName);
		$this->applyCriteria();

		if($first == true){

			if($trashed==true)
				return  $this->model->withTrashed()->where($attribute, '=', $value)->first($columns);

			return $this->model->where($attribute, '=', $value)->first($columns);
		}

		if($trashed==true)
			return  $this->model->withTrashed()->where($attribute, '=', $value)->get($columns);

		return $this->model->where($attribute, $method, $value)->get($columns);
	}


	/**
	 * Search in the records, giving the ability to add multiple where's.
	 *
	 * @param 			$searchArray
	 * @param boolean 	$first
	 * @param array 	$columns
	 * @param boolean 	$trashed
	 * @return mixed
	 */
	public function search($searchQuery, $first = false, $columns = array('*'), $trashed=false) {

		// In case we have a wrapper;
		if(count($searchQuery) == 1) $searchQuery = $searchQuery[0];
		if(count($searchQuery) != 3) throw new \Exception('Could not find proper searchArray.');

		$this->model = $this->app->make($this->modelName);
		$this->applyCriteria();

		if($first == true)
		{
			if($trashed==true)
				return  $this->model->withTrashed()->where($searchQuery[0], $searchQuery[1], $searchQuery[2])->first($columns);

			return  $this->model->where($searchQuery[0], $searchQuery[1], $searchQuery[2])->first($columns);
		}

		if($trashed==true)
			return  $this->model->withTrashed()->where($searchQuery[0], $searchQuery[1], $searchQuery[2])->get($columns);

		return  $this->model->where($searchQuery[0], $searchQuery[1], $searchQuery[2])->get($columns);
	}


	/**
	 * @return integer
	 */
	public function recordCount(){

		$query = DB::table($this->model->getTable())
			->select(DB::raw('count(*) as record_count'));

		// Apply criteria on query;
		if($this->skipCriteria !== true){
			foreach($this->getCriteria() as $criteria) {
				if($criteria instanceof Criteria)
					$query = $criteria->apply($query, $this);
			}
		}

		$recordCount = $query->get();

		if(!isset($recordCount[0])) return;

		return $recordCount[0]->record_count;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Builder
	 * @throws RepositoryException
	 */
	public function makeModel($modelName) {

		$this->modelName = $modelName;

		$model = $this->app->make($modelName);

		if (!$model instanceof Model)
			throw new RepositoryException("Class {$modelName} must be an instance of Illuminate\\Database\\Eloquent\\Model");

		return $model;
	}

	/**
	 * Get current table name
	 *
	 * @return string
	 */
	public function getTable(){
		return $this->model->getModel()->getTable();
	}

	/**
	 * Start fresh with a new baken model;
	 *
	 * @return $this
	 */
	public function resetModel(){
		$this->model = $this->makeModel($this->model());
		$this->criteria = new Collection();
		return $this;
	}

	/**
	 * Clear current criteria;
	 *
	 * @return $this
	 */
	public function cleanCriteria(){
		$this->criteria = new Collection();
		return $this;
	}

	/**
	 * @return $this
	 */
	public function resetScope() {
		$this->skipCriteria(false);
		return $this;
	}

	/**
	 * @param bool $status
	 * @return $this
	 */
	public function skipCriteria($status = true){
		$this->skipCriteria = $status;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCriteria() {
		return $this->criteria;
	}

	/**
	 * @param Criteria $criteria
	 * @return $this
	 */
	public function getByCriteria(Criteria $criteria) {
		$this->model = $criteria->apply($this->model, $this);
		return $this;
	}

	/**
	 * @param Criteria $criteria
	 * @return $this
	 */
	public function pushCriteria(Criteria $criteria) {
		$this->criteria->push($criteria);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function  applyCriteria() {
		if($this->skipCriteria === true)
			return $this;

		foreach($this->getCriteria() as $criteria) {
			if($criteria instanceof Criteria)
				$this->model = $criteria->apply($this->model, $this);
		}

		return $this;
	}
}