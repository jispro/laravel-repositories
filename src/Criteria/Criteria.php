<?php namespace Jispro\Repositories\Criteria;

use Jispro\Repositories\Contracts\RepositoryInterface as Repository;
use Jispro\Repositories\Contracts\RepositoryInterface;

abstract class Criteria {

	/**
	 * @param $model
	 * @param RepositoryInterface $repository
	 * @return mixed
	 */
	public abstract function apply($model, Repository $repository);
}